#!/usr/bin/env python

# ROS imports
import rospy
#use to load the configuration function
#use to load the PID control
#import cola2_lib
from cola2_lib import cola2_ros_lib

#import empty service to set the 0 position and the valve position
from std_srvs.srv import Empty, EmptyResponse

import numpy as np

from sensor_msgs.msg import JointState
#Publish the values readed from the master
from csip_e5_manipulator.msg import CsipE5ArmState

#active the digital output
from cola2_msgs.srv import DigitalOutput
#header
#string[] name
#float64[] position
#float64[] velocity
#float64[] effort
import serial
#need to be defined how to use the update to move the arm or what will be used,
#for instance is almost ready to use through


import threading

#import ipdb
#ipdb.set_trace()


class csipE5Arm:

    def __init__(self, name):
        self.name = name
        self.getConfig()
        rospy.loginfo('Configuration ' + str(self.name) + ' Loaded ')
        if self.working_real_auv:
            rospy.wait_for_service('digital_output')
            digital_out = rospy.ServiceProxy('digital_output', DigitalOutput)
            try:
                resp1 = digital_out(self.digital_output, True)
            except rospy.ServiceException, e:
                rospy.loginfo(("CSIP_E5_ARM: Service DigitalOutput did not" +
                               " process request: " + str(e)))
                rospy.loginfo("CSIP_E5_ARM: Shutting down node.")
                rospy.signal_shutdown("Cannot power csip_e5 manipulator")

            self.digital_output
        self.openSerialPort()
        rospy.loginfo('Serial Port of ' + str(self.name) + ' Open')
        #self.calibrate()

        self.MotorPosition = np.zeros(self.num_dof)
        self.MotorVoltatge = np.zeros(self.num_dof)
        self.MotorSpeed = np.zeros(self.num_dof)
        self.PresentPosition = np.zeros(self.num_dof)
        self.PresentSpeed = np.zeros(self.num_dof)
        self.PresentCurrent = np.zeros(self.num_dof)
        self.PresentForce = np.zeros(self.num_dof)
        self.MotorTemp = np.zeros(self.num_dof)
        self.pidMsg = self.generatePIDMsg()
        self.requestMsg = self.pidMsg
        #self.valvePose = np.zeros(self.num_dof)
        #self.publishValvePose = False
        self.counterSlew = 0
        self.temp = 0.0
        self.voltatge = 0.0
        self.current = 0.0
        self.lock_speed = threading.Lock()
        self.lock_voltatge = threading.Lock()
        self.lock_direct_speed = threading.Lock()
        self.lock_encoder = threading.Lock()
        self.high_current_counter = 0

        self.roll_value = 0.0

        if self.debug_files:
            #Debug Mode store commands positions and joints
            self.file_command_keyboard = open('command_keyboard.csv', 'w')
            self.file_current_state = open('current_state.csv', 'w')
            self.file_desired_state = open('desired_state.csv', 'w')
            self.file_speed_desired = open('speed_command.csv', 'w')
            self.file_speed_sended = open('real_speed.csv', 'w')
            #Debug the Encoders
            self.file_read_encoder = open('value_encoders.csv', 'w')
            self.file_absolute_encoder = open('absolute_encoder.csv', 'w')
            self.file_complete = open('complete_encoders.csv','w')

        #self.pid = cola2_lib.PID(self.kp, self.ti, self.td, self.fff,
        #                         rospy.Time.now().to_sec())

        #this vector will record the position of the joint in absolute encoding
        self.AbsolutePose = np.zeros(self.num_dof)

        #Old position of the Absolute pose, updated when is
        #self.OldAbsolutePose = np.zeros(self.num_dof)
        self.OldDesiredPose = np.zeros(self.num_dof)
        self.oldMotorSpeed = np.zeros(self.num_dof)
        # self.OldAbsolutePose_0 = self.AbsolutePose[0]
        # self.OldAbsolutePose_1 = self.AbsolutePose[1]
        # self.OldAbsolutePose_2 = self.AbsolutePose[2]

        #Booleans to move the joint.
        self.new_command = False
        self.movement_required = False
        self.CommandVoltatge = False
        self.CommandSpeed = False
        self.desired_joints = np.zeros(5)
        self.desired_pose = np.zeros(5)

        self.movement_sense = [True, True, True, True, True]
        self.positionReach = [True,  True, True, True, True]
        self.joint_max_limit = [False,  False, False, False, False]
        self.joint_min_limit = [False,  False, False, False, False]
        #Check if it is needed
        self.MotorSpeed = np.array([4095, 4095, -4095, 0, 0])

        self.limitCounter = 0

        self.calibrating = False
        self.firstReadingEncoders = True

        #Publish the valve position
        self.pub_joint_state = rospy.Publisher("/csip_e5_arm/joint_state",
                                               JointState,
                                               queue_size = 2)
        #Publish the configuration of the valve position Need to be configured
        # self.pub_joint_valve = rospy.Publisher(
        #     "/csip_e5_arm/joint_state_valve", JointState)

        self.pub_joint_dist = rospy.Publisher(
            "/csip_e5_arm/joint_state_dist",
            JointState,
            queue_size = 2)

        self.pub_joint_cnst = rospy.Publisher(
            "/csip_e5_arm/joint_state_coef",
            JointState,
            queue_size = 2)

        self.pub_arm_state = rospy.Publisher(
            '/csip_e5_arm/arm_state',
            CsipE5ArmState,
            queue_size = 2)

        #read the new position
        # rospy.Subscriber('/csip_e5_arm/joint_state_command',
        #                  JointState, self.move_joints_update, queue_size=1)

        #Update the new command in voltatge
        rospy.Subscriber('/csip_e5_arm/joint_voltage_command',
                         JointState, self.update_voltatge_command,
                         queue_size=1)

        #Update the new command in Speed
        rospy.Subscriber('/csip_e5_arm/joint_speed_command',
                         JointState, self.update_speed_command,
                         queue_size=1)


        #Correct the joint state
        rospy.Subscriber('/csip_e5_arm/joint_correction',
                         JointState, self.update_joint_corrections,
                         queue_size=1)

        #service to set the Zero Position
        self.set_zero_srv = rospy.Service('/csip_e5_arm/set_zero_pose',
                                          Empty, self.setZeroPose)

        #service to calibrate the arm without rebooting the node
        self.arm_calibration_srv = rospy.Service('/csip_e5_arm/arm_calibration',
                                                 Empty, self.armCalibrationSrv)

        #service to set the ValvePostion
        # self.set_valve_srv = rospy.Service(
        #     '/csip_e5_arm/set_valve_pose',
        #     Empty,
        #     self.setValvePose)

        self.set_roll_srv = rospy.Service('/csip_e5_arm/set_roll_zero',
                                          Empty, self.setRollZero)

    def setZeroPose(self, req):
        print 'Called Zero Pose '
        self.lock_encoder.acquire()
        try:
            self.AbsolutePose = np.zeros(self.num_dof)
            rospy.loginfo('Absoulte pose initialized')
        finally:
            self.lock_encoder.release()
        return EmptyResponse()

    # def setValvePose(self, req):
    #     self.valvePose = self.AbsolutePose.copy()
    #     self.publishValvePose = True
    #     rospy.loginfo('Valve set sucessfully')
    #     return EmptyResponse()

    def setRollZero(self, req):
        print 'Called Zero Roll '
        self.lock_encoder.acquire()
        try:
            rospy.loginfo('The Roll is setted Zero')
            self.AbsolutePose[3] = 0.0
        finally:
            self.lock_encoder.release()
        return EmptyResponse()

    def getConfig(self):
        param_dict = {'sp_path': '/csip_e5_arm/sp_path',
                      'sp_baud_rate': '/csip_e5_arm/sp_baud_rate',
                      'sp_char_size': '/csip_e5_arm/sp_char_size',
                      'sp_stop_bits': '/csip_e5_arm/sp_stop_bits',
                      'sp_parity': '/csip_e5_arm/sp_parity',
                      'sp_timeout': '/csip_e5_arm/sp_timeout',
                      'sp_xonxoff': '/csip_e5_arm/sp_xonxoff',
                      'pid_pose_p': '/csip_e5_arm/pid_pose/p',
                      'pid_pose_i': '/csip_e5_arm/pid_pose/i',
                      'pid_pose_d': '/csip_e5_arm/pid_pose/d',
                      'pid_speed_p': '/csip_e5_arm/pid_speed/p',
                      'pid_speed_i': '/csip_e5_arm/pid_speed/i',
                      'pid_speed_d': '/csip_e5_arm/pid_speed/d',
                      'speed_limit': '/csip_e5_arm/speed_limit',
                      'real_speed_limit': 'csip_e5_arm/real_speed_limit',
                      'current_limit': '/csip_e5_arm/current_limit',
                      'force_limit': '/csip_e5_arm/force_limit',
                      'period': '/csip_e5_arm/period',
                      'num_dof': '/csip_e5_arm/num_dof',
                      'initial_pose': '/csip_e5_arm/initial_pose',
                      'fff': 'csip_e5_arm/pid/feed_forward_force',
                      'kp': 'csip_e5_arm/pid/kp',
                      'ti': 'csip_e5_arm/pid/ti',
                      'td': 'csip_e5_arm/pid/td',
                      'calibration_needed': 'csip_e5_arm/calibration_needed',
                      'calibration_file': 'csip_e5_arm/calibration_file',
                      'conversion': 'csip_e5_arm/conv',
                      'max_current': 'csip_e5_arm/max_current',
                      'max_limit_counter': 'csip_e5_arm/max_limit_counter',
                      'joints_enable': 'csip_e5_arm/joints_enable',
                      'cali_positive_sense': 'csip_e5_arm/cali_positive_sense',
                      'digital_output': 'csip_e5_arm/digital_output',
                      'working_real_auv': 'csip_e5_arm/working_real_auv',
                      'debug_files': 'csip_e5_arm/debug_files',
                      'max_step' : 'csip_e5_arm/max_step',
                      'r0' : 'csip_e5_arm/r0',
                      'R0': 'csip_e5_arm/R0',
                      'x0_min':'csip_e5_arm/x0_min',
                      'x0_max': 'csip_e5_arm/x0_max' ,
                      'q0_min':'csip_e5_arm/q0_min',
                      'q0_max' : 'csip_e5_arm/q0_max',
                      'enc_inc0' : 'csip_e5_arm/enc_inc0',
                      'r1' : 'csip_e5_arm/r1',
                      'R1': 'csip_e5_arm/R1',
                      'x1_min':'csip_e5_arm/x1_min',
                      'x1_max': 'csip_e5_arm/x1_max' ,
                      'q1_min':'csip_e5_arm/q1_min',
                      'q1_max' : 'csip_e5_arm/q1_max',
                      'enc_inc1' : 'csip_e5_arm/enc_inc1',
                      'r2' : 'csip_e5_arm/r2',
                      'R2': 'csip_e5_arm/R2',
                      'x2_min':'csip_e5_arm/x2_min',
                      'x2_max': 'csip_e5_arm/x2_max' ,
                      'q2_min':'csip_e5_arm/q2_min',
                      'q2_max' : 'csip_e5_arm/q2_max',
                      'enc_inc2' : 'csip_e5_arm/enc_inc2'
                      }
        cola2_ros_lib.getRosParams(self, param_dict)
        self.computeParametersCalibration()

    def computeParametersCalibration(self):
        self.q0_min_val = (np.pi/180)*(sum(self.q0_min))/len(self.q0_min)
        self.q0_max_val = (np.pi/180)*(sum(self.q0_max))/len(self.q0_max)
        self.q0bar_min = np.pi-self.q0_min_val-np.arccos((self.r0**2+self.R0**2-self.x0_max**2)/(2*self.r0*self.R0))
        self.q0bar_max = np.pi-self.q0_max_val-np.arccos((self.r0**2+self.R0**2-self.x0_min**2)/(2*self.r0*self.R0))
        self.q0bar = (self.q0bar_min+self.q0bar_max)/2
        self.enc_inc0_val = (sum(self.enc_inc0)/len(self.enc_inc0))
        self.x_inc0 = (self.x0_max-self.x0_min)/self.enc_inc0_val

        self.q1_min_val = (np.pi/180)*(sum(self.q1_min))/len(self.q1_min)
        self.q1_max_val = np.pi-(np.pi/180)*(sum(self.q1_max))/len(self.q1_min)
        self.q1bar_min = np.pi-self.q1_min_val-np.arccos((self.r1**2+self.R1**2-self.x1_max**2)/(2*self.r1*self.R1))
        self.q1bar_max = np.pi-self.q1_max_val-np.arccos((self.r1**2+self.R1**2-self.x1_min**2)/(2*self.r1*self.R1))
        self.q1bar = (self.q1bar_min+self.q1bar_max)/2.0
        self.enc_inc1_val = (sum(self.enc_inc1)/len(self.enc_inc1))
        self.x_inc1 = (self.x1_max-self.x1_min)/self.enc_inc1_val

        self.q2_min_val = -np.pi/2.0 + (np.pi/180)*(sum(self.q2_min))/len(self.q2_min)
        self.q2_max_val = np.pi/2.0 - (np.pi/180)*(sum(self.q2_max))/len(self.q2_max)
        self.q2bar_min = np.pi-self.q2_min_val+np.arccos((self.r2**2+self.R2**2-self.x2_max**2)/(2*self.r2*self.R2))
        self.q2bar_max = np.pi-self.q2_max_val+np.arccos((self.r2**2+self.R2**2-self.x2_min**2)/(2*self.r2*self.R2))
        self.q2bar = (self.q2bar_min+self.q2bar_max)/2.0
        self.enc_inc2_val = (sum(self.enc_inc2)/len(self.enc_inc2))
        self.x_inc2 = (self.x2_max-self.x2_min)/self.enc_inc2_val

    def initInternalValues(self):
        for i in xrange(2):
            try:
                self.sp.write(self.pidMsg)
            except serial.serialutil.SerialTimeoutException:
                rospy.loginfo('Error Writing Serial Port')
                self.openSerialPort()
                rospy.loginfo('Serial Port Reopened')
            line = self.sp.read(51)
            self.readAnswerArm(line)
            rospy.sleep(self.period)
        rospy.loginfo('init Internal Values')

    def storeCalibration(self):
        file = open(self.calibration_file, 'w')
        for i in xrange(self.num_dof):
            file.write('Joint'+str(i) + ' \n' +
                       str(self.AbsolutePose[i]) + ' \n')
        file.close()
        rospy.loginfo('Position Store')

    def loadCalibration(self):
        print ' Called Load Calibration '
        try:
            calibrationFile = open(self.calibration_file, 'r').readlines()
            calibrationFile = [word.strip() for word in calibrationFile]
            j = 0
            self.lock_encoder.acquire()
            try:
                for i in xrange(len(calibrationFile)):
                    if calibrationFile[i] == 'Joint'+str(j):
                        i += 1
                        self.AbsolutePose[j] = float(calibrationFile[i])
                        j += 1
                    else:
                        pass
                rospy.loginfo('Position Loaded')
            finally:
                self.lock_encoder.release()
        except IOError:
            rospy.logerr(
                'The file ' +
                self.calibration_file +
                'is not find, init with 0')

    def openSerialPort(self):
        self.sp = serial.Serial(self.sp_path, self.sp_baud_rate,
                                bytesize=self.sp_char_size,
                                parity=self.sp_parity,
                                stopbits=self.sp_stop_bits,
                                timeout=self.sp_timeout,
                                xonxoff=self.sp_xonxoff,
                                writeTimeout=0.5)

    #Experimental Distances with
    # Slew 70181 - Step size 2.864E-5
    # Elevation 71413 - Step size 2.0725E-5
    # Elbow 66611 - Step size 3.043
    #This method move the arm to the limit of each joint.
    def calibrate(self):
        #self.MotorVoltatge[i]
        # Using the voltatge
        # For the moment we avoid the calibration of the roll.
        self.calibrating = True
        for i in xrange(self.num_dof-2):
            limitReached = False
            valuesLimits = np.array([0, 0, 0, 0, 0])
            try:
                self.sp.write(self.pidMsg)
            except serial.serialutil.SerialTimeoutException:
                rospy.loginfo('Error Writing Serial Port')
                self.openSerialPort()
                rospy.loginfo('Serial Port Reopened')
            line = self.sp.read(51)
            if not self.joints_enable[i]:
                rospy.loginfo('Motor number ' + str(i) + 'disable')
                self.AbsolutePose[i] = 0
                limitReached = True
            elif (self.readAnswerArm(line) == 0):
                if self.current > self.max_current:
                    limitReached = True
                    self.AbsolutePose[i] = valuesLimits[i]
            self.lock_speed.acquire()
            try:
                for j in xrange(self.num_dof):
                    self.MotorSpeed[j] = 0
                rospy.loginfo('Looking for the limit of the motor ' + str(i))
            finally:
                self.lock_speed.release()
            while not limitReached and not rospy.is_shutdown():
                self.lock_speed.acquire()
                try:
                    if self.cali_positive_sense[i]:
                        #self.MotorVoltatge[i] = 65535
                        self.MotorSpeed[i] = 4095
                    else:
                        #self.MotorVoltatge[i] = -65535
                        self.MotorSpeed[i] = -4095
                finally:
                    self.lock_speed.release()
                #self.requestMsg = self.generateVoltatgeMsg()
                self.requestMsg = self.generateSpeedMsg()
                try:
                    self.sp.write(self.requestMsg)
                except serial.serialutil.SerialTimeoutException:
                    rospy.loginfo('Error Writing Serial Port')
                    self.openSerialPort()
                    rospy.loginfo('Serial Port Reopened')
                line = self.sp.read(51)
                self.time_read = rospy.get_time()
                if (self.readAnswerArm(line) == 0):
                    #file.write(s)
                    if(self.current >= self.max_current
                       and self.limitCounter >= self.max_limit_counter):
                        #before unlock the joint set the position.
                        self.AbsolutePose[i] = valuesLimits[i]
                        #unlock the joint
                        rospy.loginfo('Unlock the joint')
                        iterations = 0
                        while ((self.current >= self.max_current or
                                iterations < 10) and
                               not rospy.is_shutdown()):
                            if self.cali_positive_sense[i]:
                                self.MotorVoltatge[i] = -32767
                            else:
                                self.MotorVoltatge[i] = 32767
                            self.requestMsg = self.generateVoltatgeMsg()
                            try:
                                self.sp.write(self.requestMsg)
                            except serial.serialutil.SerialTimeoutException:
                                rospy.loginfo('Error Writing Serial Port')
                                self.openSerialPort()
                                rospy.loginfo('Serial Port Reopened')
                            line = self.sp.read(51)
                            self.readAnswerArm(line)
                            iterations = iterations + 1
                        limitReached = True
                        self.MotorVoltatge[i] = 0
                        self.limitCounter = 0
                    elif(self.current >= self.max_current
                         and self.limitCounter < self.max_limit_counter):
                        self.limitCounter += 1
                    else:
                        self.limitCounter = 0
                else:
                    rospy.logerr('Error reading the message')
                rospy.sleep(self.period)
            rospy.sleep(self.period)
            if rospy.is_shutdown():
                break
            #file.close()
        rospy.loginfo('all the limits reached')
        self.calibrating = False
        #set zero to the AbsolutePose
        #Elevation 1.4687 -> Conv 43808.215  -> 64341
        #Slew -0.5818 -> Conv 32442.3484  -> -18875
        #Elbow -1.4328 -> Conv 30502.5789  -> -43704
        #WARNING [Elevation, Slew, Elbow]

    def armCalibrationSrv(self, req):
        """
        This method call the function to calibrate the arm.
        """
        self.calibrate()
        return EmptyResponse()

    def publish_joints(self):
        try:
            self.sp.write(self.pidMsg)
        except serial.serialutil.SerialTimeoutException:
            rospy.loginfo('Error Writing Serial Port')
            self.openSerialPort()
            rospy.loginfo('Serial Port Reopened')
        line = self.sp.read(51)
        self.readAnswerArm(line)

        joint_state = JointState()
        #32442.3484
        conv = np.array(self.conversion)
        #WORK AROUND -> Tu avoid the 0 of the las parameter
        conv[0:4] = 1/conv[0:4]

        #Greek code
        # rospy.loginfo('AbsolutePose ' +str(self.AbsolutePose[2]) )
        # rospy.loginfo(' X inc 2 ' + str(self.x_inc2))
        # rospy.loginfo(' X2 min ' + str(self.x2_min))
        self.lock_encoder.acquire()
        try:
            self.x0_dist = self.x0_min + self.AbsolutePose[0]*self.x_inc0
            self.x1_dist = self.x1_min + self.AbsolutePose[1]*self.x_inc1
            self.x2_dist = self.x2_min + self.AbsolutePose[2]*self.x_inc2
        finally:
            self.lock_encoder.release()


        #Publish the distance
        joint_dist = JointState()
        joint_dist.position = [ self.x1_dist, self.x0_dist, self.x2_dist, 0.0 , 0.0 ]
        self.pub_joint_dist.publish(joint_dist)

        #Publish the constant derivative
        # R0 = 0.21900
        # r0 = 0.03749
        # x0_dist = 0.240095
        # x0_value = - ((R0*r0)*(np.sqrt(1-((R0**2+r0**2-x0_dist**2)/(2*R0*r0))**2)))/x0_dist
        x0_value = ((self.R0*self.r0)*(np.sqrt(1-((self.R0**2+self.r0**2-self.x0_dist**2)/(2*self.R0*self.r0))**2)))/self.x0_dist
        x1_value = ((self.R1*self.r1)*(np.sqrt(1-((self.R1**2+self.r1**2-self.x1_dist**2)/(2*self.R1*self.r1))**2)))/self.x1_dist
        x2_value = ((self.R2*self.r2)*(np.sqrt(1-((self.R2**2+self.r2**2-self.x2_dist**2)/(2*self.R2*self.r2))**2)))/self.x2_dist
        joint_cnst = JointState()
        joint_cnst.position = [ x1_value, x0_value, x2_value, 0.0, 0.0 ]
        self.pub_joint_cnst.publish(joint_cnst)

        self.lock_encoder.acquire()
        try:
            roll_angle = self.AbsolutePose[3]*conv[3]
        finally:
            self.lock_encoder.release()


        # if (roll_angle > np.pi or
        #     roll_angle < -np.pi):
        #     roll_angle = cola2_lib.wrapAngle(roll_angle)
        #     self.AbsolutePose[3] = int(roll_angle/conv[3])
        #rospy.loginfo('Roll value ' +str(roll_angle))
        #q1 = pi-q1bar-acos((r1^2+R1^2-x1.^2)/(2*r1*R1));
        q0_val = np.pi-self.q0bar-np.arccos((self.r0**2+self.R0**2-self.x0_dist**2)/(2*self.r0*self.R0))
        q1_val = np.pi-self.q1bar-np.arccos((self.r1**2+self.R1**2-self.x1_dist**2)/(2*self.r1*self.R1))
        q2_val = np.pi-self.q2bar+np.arccos((self.r2**2+self.R2**2-self.x2_dist**2)/(2*self.r2*self.R2))

        self.roll_value = roll_angle
        joint_state.position = [q0_val, q1_val, q2_val, roll_angle, 0]
        joint_state.velocity = self.PresentSpeed.tolist()
        joint_state.effort = self.PresentForce.tolist()

        #x0_dist = np.sqrt(-1*((np.cos(-q0_val-np.pi-self.q0bar)*2*self.r0*self.R0)-(self.r0**2)-(self.R0**2)))
        #x1_dist = np.sqrt(-1*((np.cos(-q1_val-np.pi-self.q1bar)*2*self.r1*self.R1)-(self.r1**2)-(self.R1**2)))
        #x2_dist = np.sqrt(-1*((np.cos(q2_val-np.pi+self.q2bar)*2*self.r2*self.R2)-(self.r2**2)-(self.R2**2)))

        #rospy.loginfo('x0_dist ' + str(x0_dist) + ' real ' + str(self.x0_dist))
        #rospy.loginfo('x1_dist ' + str(x1_dist) + ' real ' + str(self.x1_dist))
        #rospy.loginfo('x2_dist ' + str(x2_dist) + ' real ' + str(self.x2_dist))

        # rospy.loginfo("Current command %1.2f,  %1.2f,  %1.2f"
        #               % (joint_state.position[0],
        #                  joint_state.position[1],
        #                  joint_state.position[2]))
        if self.debug_files:
            self.file_current_state.write(str(rospy.get_time()) + ', ' +
                                          str(joint_state.position[0]) + ', ' +
                                          str(joint_state.position[1]) + ', ' +
                                          str(joint_state.position[2]) + ', ' +
                                          str(joint_state.position[3]) + '\n')

        #swap the first with the second
        aux = joint_state.position[0]
        joint_state.position[0] = joint_state.position[1]
        joint_state.position[1] = aux

        aux = joint_state.velocity[0]
        joint_state.velocity[0] = joint_state.velocity[1]
        joint_state.velocity[1] = aux

        aux = joint_state.effort[0]
        joint_state.effort[0] = joint_state.effort[1]
        joint_state.effort[1] = aux

        #finish the swaping area

        joint_state.header.stamp = rospy.get_rostime()
        self.pub_joint_state.publish(joint_state)

        #publish the arm state
        arm_state = CsipE5ArmState()
        arm_state.header.stamp = rospy.get_rostime()
        arm_state.temperature = self.temp
        arm_state.voltatge = self.voltatge
        arm_state.current = self.current
        arm_state.joint_max_limit = self.joint_max_limit
        arm_state.joint_min_limit = self.joint_min_limit
        self.pub_arm_state.publish(arm_state)
        #swap the first with the second
#         if self.publishValvePose:
#             valve_state = JointState()
#             conv = np.array(self.conversion)
#             #WORK AROUND -> Tu avoid the 0 of the las parameter
#             conv[0:4] = 1/conv[0:4]
#             # conv = np.array([1/43808.215,
#             #                  1/32442.3484,
#             #                  1/30502.5789,
#             #                  1/503.9809,
#             #                  0])
#             valve_state.position = (self.valvePose*conv).tolist()
# #            rospy.loginfo('Valve pose ' + str(self.valvePose))
#             aux = valve_state.position[0]
#             valve_state.position[0] = valve_state.position[1]
#             valve_state.position[1] = aux
#             self.pub_joint_valve.publish(valve_state)

    def move_joints_update_fake(self, data):
        self.CommandVoltatge = False
        self.CommandSpeed = False
        self.new_command = True
        self.movement_required = True
         #conv = np.array([43808.215, 32442.3484, 30502.5789, 503.9809, 0])
        #pendent TODO
        self.computeMotorsSpeed()

    def computeMotorsSpeed(self):
        self.lock_speed.acquire()
        try:
            # rospy.loginfo("Current State " + str(self.AbsolutePose[0])
            #               + ", " +
            #               str(self.AbsolutePose[1]) + ", " +
            #               str(self.AbsolutePose[2]) + ", " +
            #               str(self.AbsolutePose[3]))
            self.lock_encoder.acquire()
            try:
                inc_cmd = np.abs(self.desired_pose - self.AbsolutePose)
            finally:
                self.lock_encoder.release()

            # rospy.loginfo('Increment ' + str(inc_cmd[0]) + ', ' +
            #               str(inc_cmd[1]) + ', ' +
            #               str(inc_cmd[2]) + ', ' +
            #               str(inc_cmd[3]))
            time = inc_cmd / self.real_speed_limit
            max_index = np.argmax(time)

            self.oldMotorSpeed = np.copy(self.MotorSpeed)
            if sum(np.zeros(5) == inc_cmd) != 5:
#            self.lock_speed.acquire()
#            try:
                #self.oldMotorSpeed = np.copy(self.MotorSpeed)
                # rospy.loginfo("Time " + str(time[0]) + ', ' +
                #               str(time[1]) + ', ' + str(time[2]) +
                #               ', ' + str(time[3]) +
                #               ', Time Max ' + str(max_index))
                self.MotorSpeed = np.round((inc_cmd / time[max_index]) *
                                           (np.asarray(self.speed_limit) /
                                            np.asarray(self.real_speed_limit)))
                #rospy.loginfo('Motor Skill computed ' + str(self.MotorSpeed))
                if self.MotorSpeed[max_index] > self.speed_limit[max_index]:
                    self.MotorSpeed[max_index] = self.speed_limit[max_index]
                #Work around because one motor goes in another movement sense
                self.MotorSpeed[2] = -1*self.MotorSpeed[2]
                #disable the speed of the motors not enables
                for i in xrange(self.num_dof):
                    if not self.joints_enable[i]:
                        self.MotorSpeed[i] = 0.0
                #rospy.loginfo('Motor Speed ' + str(self.MotorSpeed))
                # rospy.loginfo("Desired Speed " +
                #               str(self.MotorSpeed[0]) + ", " +
                #               str(self.MotorSpeed[1]) + ", " +
                #               str(self.MotorSpeed[2]) + ", " +
                #               str(self.MotorSpeed[3]))
            #finally:
            #    self.lock_speed.release()
            else:
            #rospy.loginfo('Clean the data!!!!!!!!!!!!!!');
                #self.lock_speed.acquire()
                #try:
                self.MotorSpeed = np.zeros(5)
                #finally:
                #self.lock_speed.release()
            #rospy.loginfo('Motor Speed ' + str(self.MotorSpeed))
            #Compute movement sense
            self.lock_encoder.acquire()
            try:
                self.movement_sense = self.desired_pose > self.AbsolutePose
            finally:
                self.lock_encoder.release()
            for i in xrange(self.num_dof):
                if not self.movement_sense[i]:
                    self.MotorSpeed[i] = -1*self.MotorSpeed[i]

            #Protection Agains Peaks
            # dif_abs_desired = self.OldAbsolutePose[self.OldAbsolutePose !=
            #                                        self.OldAbsolutePose[
            #                                            max_index]]
            # dif_abs_current = self.AbsolutePose[self.AbsolutePose !=
            #                                     self.AbsolutePose[max_index]]
            # dif_des_old = self.OldDesiredPose[self.OldDesiredPose !=
            #                                   self.OldDesiredPose[max_index]]
            # dif_des_new = self.desired_pose[self.desired_pose !=
            #                                 self.desired_pose[max_index]]
            # if(np.shape(dif_abs_desired)[0] != 0 and
            #    np.shape(dif_abs_current)[0] != 0 and
            #    np.shape(dif_des_old)[0] != 0 and
            #    np.shape(dif_des_new)[0] != 0):
            #     dif_absolute = (dif_abs_desired - dif_abs_current)
            #     dif_desired = (dif_des_old - dif_des_new)
            #     if (any(abs(dif_absolute - dif_desired) > 30)):
            #         self.MotorSpeed = np.copy(self.oldMotorSpeed)

            # if any(abs(self.MotorSpeed - self.oldMotorSpeed) > 200):
            #     rospy.loginfo('!!!!!The protection has been not usefull!!!!!!')
            #     rospy.loginfo("Current Speed " +
            #                   str(self.MotorSpeed[0]) + ", " +
            #                   str(self.MotorSpeed[1]) + ", " +
            #                   str(self.MotorSpeed[2]))
            #     rospy.loginfo("Old Speed " +
            #                   str(self.oldMotorSpeed[0]) + ", " +
            #                   str(self.oldMotorSpeed[1]) + ", " +
            #                   str(self.oldMotorSpeed[2]))
            # self.lock_encoder.acquire()
            # try:
            #     self.OldAbsolutePose = np.copy(self.AbsolutePose)
            # finally:
            #     self.lock_encoder.release()
            self.OldDesiredPose = np.copy(self.desired_pose)
            if self.debug_files:
                self.file_speed_desired.write(str(rospy.get_time()) + ", " +
                                              str(self.MotorSpeed[0]) + ", " +
                                              str(self.MotorSpeed[1]) + ", " +
                                              str(self.MotorSpeed[2]) + ", " +
                                              str(self.MotorSpeed[3]) + '\n')
        finally:
            self.lock_speed.release()

    def move_joints_update(self, data):
        self.new_command = True
        self.movement_required = True
        self.CommandVoltatge = False
        self.CommandSpeed = False
        #swap the first with the second two put in the same order than
        positions = np.array([data.position[1], data.position[0],
                              data.position[2], data.position[3],
                              data.position[4]])
        if self.debug_files:
            self.file_command_keyboard.write("%1.3f, %1.2f, %1.2f, %1.2f, %1.2f\n"
                                             % (rospy.get_time(), positions[0],
                                                positions[1], positions[2],
                                                positions[3]))

        rospy.loginfo("Keyboard Command " + str(positions[0]) + ", " +
                      str(positions[1]) + ", " + str(positions[2])
                      + ', ' + str(positions[3]))
        #positions = positions
        conv = np.array(self.conversion)
        #conv = np.array([43808.215, 32442.3484, 30502.5789, 503.9809, 0])
        new_desired_pose = positions*conv
        if (any(abs(new_desired_pose - self.desired_pose) > 5)):
            self.new_command = True
            self.movement_required = True
            self.desired_pose = np.copy(new_desired_pose)
            rospy.loginfo("Desired State " + str(self.desired_pose[0]) + ", " +
                          str(self.desired_pose[1]) + ", " +
                          str(self.desired_pose[2]) + ', ' +
                          str(self.desired_pose[3]))
        # rospy.loginfo("Desired command %1.2f,  %1.2f,  %1.2f"
        #               % (positions[0], positions[1], positions[2]))
            if self.debug_files:
                self.file_desired_state.write("%1.3f, %1.2f,  %1.2f, %1.2f, %1.2f\n"
                                              % (rospy.get_time(),
                                                 self.desired_pose[0],
                                                 self.desired_pose[1],
                                                 self.desired_pose[2],
                                                 self.desired_pose[3]))
        #Protection to aboid small movements
        # for i in xrange(self.num_dof):
        #    if abs(self.AbsolutePose[i] - self.desired_pose[i]) < 100:
        #        self.desired_pose[i] = self.AbsolutePose[i]

            self.computeMotorsSpeed()

        #TODO:: WORK AROUND
        # if self.AbsolutePose[1] != self.desired_pose[1]:
        #     if self.counterSlew < 3:
        #         self.desired_pose[1] = self.AbsolutePose[1]
        #         self.counterSlew += 1
        # else:
        #     self.counterSlew = 0

        #rospy.loginfo('Current joints ' + str(self.AbsolutePose))
        #rospy.loginfo('Desired joints ' + str(self.desired_pose))

    def update_voltatge_command(self, data):
        self.CommandVoltatge = True
        self.CommandSpeed = False
        self.new_command = False
        self.movement_required = False
        self.lock_voltatge.acquire()
        try:
            self.MotorVoltatge = np.array([data.position[1], data.position[0],
                                           -data.position[2], data.position[3],
                                           data.position[4]])
            for i in range(5):
                if (self.MotorVoltatge[i] > 65534):
                    self.MotorVoltatge[i] = 65534
                elif (self.MotorVoltatge[i] < -65534):
                    self.MotorVoltatge[i] = -65534
#            rospy.loginfo('Motor Voltatge Command ' + str(self.MotorVoltatge[0]) + ', ' + str(self.MotorVoltatge[1]) + ', ' + str(self.MotorVoltatge[2]) + ', ' + str(self.MotorVoltatge[3]) + ', ' + str(self.MotorVoltatge[1]))
        finally:
            self.lock_voltatge.release()

    def update_speed_command(self, data):
        self.CommandVoltatge = False
        self.CommandSpeed = True
        self.new_command = False
        self.movement_required = False
        self.lock_direct_speed.acquire()
        try:
            self.MotorSpeed[0] = (((data.position[1]
                                    * self.real_speed_limit[0])
                                   / self.speed_limit[0])
                                  * 60)
            if (self.MotorSpeed[0] > self.speed_limit[0]):
                self.MotorSpeed[0] = self.speed_limit[0]
            elif (self.MotorSpeed[0] < -self.speed_limit[0]):
                self.MotorSpeed[0] = -self.speed_limit[0]

            self.MotorSpeed[1] = (((data.position[0]
                                    * self.real_speed_limit[1])
                                   / self.speed_limit[1])
                                  * 60)
            if (self.MotorSpeed[1] > self.speed_limit[1]):
                self.MotorSpeed[1] = self.speed_limit[1]
            elif (self.MotorSpeed[1] < -self.speed_limit[1]):
                self.MotorSpeed[1] = -self.speed_limit[1]

            self.MotorSpeed[2] = -(((data.position[2]
                                     * self.real_speed_limit[2])
                                    / self.speed_limit[2])
                                   * 60)
            if (self.MotorSpeed[2] > self.speed_limit[2]):
                self.MotorSpeed[2] = self.speed_limit[2]
            elif (self.MotorSpeed[2] < -self.speed_limit[2]):
                self.MotorSpeed[2] = -self.speed_limit[2]

            self.MotorSpeed[3] = (((data.position[3]
                                    * self.real_speed_limit[3])
                                   / self.speed_limit[3])
                                  *60)
            if (self.MotorSpeed[3] > self.speed_limit[3]):
                self.MotorSpeed[3] = self.speed_limit[3]
            elif (self.MotorSpeed[3] < -self.speed_limit[3]):
                self.MotorSpeed[3] = -self.speed_limit[3]

            self.MotorSpeed[4] = 0.0
            rospy.loginfo('Motor Speed Received ' + str(self.MotorSpeed[0])
                          + ',' + str(self.MotorSpeed[1]) + ',' +
                          str(self.MotorSpeed[2]) + ',' +
                          str(self.MotorSpeed[3]))
        finally:
            self.lock_direct_speed.release()

    def update_joint_corrections(self, correction):
        """
        This method replace the current pose of the joints with the new one
        @param odometry: Contains the new values for the joints
        @type odometry: JointState
        """
        #Original equations to compute the q values
        # self.x0_dist = self.x0_min + self.AbsolutePose[0]*self.x_inc0
        # self.x1_dist = self.x1_min + self.AbsolutePose[1]*self.x_inc1
        # self.x2_dist = self.x2_min + self.AbsolutePose[2]*self.x_inc2

        # q0_val = np.pi-self.q0bar-np.arccos((self.r0**2+self.R0**2-self.x0_dist**2)/(2*self.r0*self.R0))
        # q1_val = np.pi-self.q1bar-np.arccos((self.r1**2+self.R1**2-self.x1_dist**2)/(2*self.r1*self.R1))
        # q2_val = np.pi-self.q2bar+np.arccos((self.r2**2+self.R2**2-self.x2_dist**2)/(2*self.r2*self.R2))

        #roll_angle = self.AbsolutePose[3]*conv[3]

        print 'Joint Correction '
        q0_new = correction.position[1]
        q1_new = correction.position[0]
        q2_new = correction.position[2]
        q3_new = correction.position[3]
        #Inverted equations
        self.lock_encoder.acquire()
        try:
            if q0_new == -999.0 and q1_new == -999.0 and q2_new == -999.0:
                self.AbsolutePose[3] =  q3_new * self.conversion[3]
            else:
                x0_dist = np.sqrt( (self.r0**2) + (self.R0**2)  -
                                   ((2*self.r0*self.R0)* np.cos(
                                       np.pi - self.q0bar - q0_new)))
                self.AbsolutePose[0] = (x0_dist - self.x0_min) / self.x_inc0
                # rospy.loginfo('Current Abs 0 ' + str(self.AbsolutePose[0]))
                # rospy.loginfo('Desired Abs 0 ' + str((x0_dist - self.x0_min) / self.x_inc0))

                x1_dist = np.sqrt( (self.r1**2) + (self.R1**2)  -
                                   (2*self.r1*self.R1)* np.cos(
                                       np.pi - self.q1bar - q1_new))
                self.AbsolutePose[1] = (x1_dist - self.x1_min) / self.x_inc1
                # rospy.loginfo('Current Abs 1 ' + str(self.AbsolutePose[1]))
                # rospy.loginfo('Desired Abs 1 ' + str((x1_dist - self.x1_min) / self.x_inc1))

                x2_dist = np.sqrt( (self.r2**2) + (self.R2**2)  -
                                   (2*self.r2*self.R2)* np.cos(
                                       self.q2bar + q2_new - np.pi))

                self.AbsolutePose[2] = (x2_dist - self.x2_min) / self.x_inc2
                # rospy.loginfo('Current Abs 2 ' + str(self.AbsolutePose[2]))
                # rospy.loginfo('Desired Abs 2 ' + str((x2_dist - self.x2_min) / self.x_inc2))
                self.AbsolutePose[3] =  q3_new * self.conversion[3]
                s = (str(rospy.get_time()) + ', ' +
                     str(self.PresentPosition[0]) + ', ' +
                     str(self.PresentPosition[0]) + ', ' +
                     str(0.0) + ', ' +
                     str((x0_dist - self.x0_min) / self.x_inc0) + ', ' +
                     str(self.AbsolutePose[0]) + ', ' +
                     'LandMark' + ', ' +
                     str(rospy.get_time()) + ', ' +
                     str(self.PresentPosition[1]) + ', ' +
                     str(self.PresentPosition[1]) + ', ' +
                     str(0.0) + ', ' +
                     str((x1_dist - self.x1_min) / self.x_inc1) + ', ' +
                     str(self.AbsolutePose[1]) + ', ' +
                     'LandMark' + ', ' +
                     str(rospy.get_time()) + ', ' +
                     str(self.PresentPosition[2]) + ', ' +
                     str(self.PresentPosition[2]) + ', ' +
                     str(0.0) + ', ' +
                     str((x2_dist - self.x2_min) / self.x_inc2) + ', ' +
                     str(self.AbsolutePose[2]) + ', ' +
                     'LandMark' + ', ' +
                     str(rospy.get_time()) + ', ' +
                     str(self.PresentPosition[3]) + ', ' +
                     str(self.PresentPosition[3]) + ', ' +
                     str(0.0) + ', ' +
                     str(self.AbsolutePose[3]) + ', ' +
                     str(self.AbsolutePose[3]) + ', ' +
                     'Normal' + '\n')
                if self.debug_files:
                    self.file_complete.write(s)
        finally:
            self.lock_encoder.release()


    def shutdown_digital_output(self):
        rospy.wait_for_service('digital_output')
        digital_out = rospy.ServiceProxy('digital_output', DigitalOutput)
        try:
            resp1 = digital_out(self.digital_output, False)
        except rospy.ServiceException, e:
            rospy.loginfo(("CSIP_E5_ARM: Service DigitalOutput did not " +
                           " process request: " + str(e)))

    def run(self):
        while not rospy.is_shutdown():
            if not self.calibrating:
                self.publish_joints()
                if(self.current >= self.max_current and self.high_current_counter < 5 ) :
                    rospy.logerr('WARNING INTENSITY TO HIGH ' + str(self.high_current_counter))
                    self.high_current_counter += 1
                    rospy.logerr('JOINT IN MAXIMUM INTENSITY IS ' + str(np.argmax(self.PresentForce)))
                    self.joint_min_limit[np.argmax(self.PresentForce)] = True
                elif(self.current >= self.max_current and self.high_current_counter >= 5):
                    rospy.logerr('STOP EVERY THING!!!!!!!!!!')
                    self.storeCalibration()
                    if self.debug_files:
                        self.file_command_keyboard.close()
                        self.file_current_state.close()
                        self.file_desired_state.close()
                        self.file_speed_desired.close()
                        self.file_speed_sended.close()
                        self.file_read_encoder.close()
                        self.file_absolute_encoder.close()
                        self.file_complete.close()
                    self.sp.close()
                    rospy.logerr('Closing digital Output !!!!!!!!!!')
                    rospy.wait_for_service('digital_output')
                    digital_out = rospy.ServiceProxy('digital_output', DigitalOutput)
                    try:
                        resp1 = digital_out(self.digital_output, False)
                    except rospy.ServiceException, e:
                        rospy.loginfo(("CSIP_E5_ARM: Service DigitalOutput did not " +
                                       " process request: " + str(e)))
                    rospy.signal_shutdown('You can burn every THING')
                else:
                    self.high_current_counter = 0
            #move if is required
                self.move_joints_v2()
            rospy.sleep(self.period)
        self.storeCalibration()
        if self.debug_files:
            self.file_command_keyboard.close()
            self.file_current_state.close()
            self.file_desired_state.close()
            self.file_speed_desired.close()
            self.file_speed_sended.close()
            self.file_read_encoder.close()
            self.file_absolute_encoder.close()
        self.sp.close()

    def checkJntLimitVoltage(self):
        self.joint_max_limit = [False,  False, False, False, False]
        self.joint_min_limit = [False,  False, False, False, False]
        #segurity band for each joint 5 %
        seg_band0 = (self.x0_max - self.x0_min)*0.05
        seg_band1 = (self.x1_max - self.x1_min)*0.05
        seg_band2 = (self.x2_max - self.x2_min)*0.05
        seg_band3 = (np.pi*2)*0.05
         #rospy.loginfo('Voltatge ' + str(self.MotorVoltatge[1]))
        # rospy.loginfo('Joint 0: Min ' + str(self.x1_min + seg_band1) +
        #               ' < ' + str(self.x1_dist) + ' < ' +
        #               str(self.x1_max - seg_band1) + ' Voltatge '
        #               + str(self.MotorVoltatge[1]))
        # rospy.loginfo('Joint 1: Min ' + str(self.x0_min + seg_band0) +
        #               ' < ' + str(self.x0_dist) + ' < ' +
        #               str(self.x0_max - seg_band0) + ' Voltatge '
        #               + str(self.MotorVoltatge[0]))
        # rospy.loginfo('Joint 2: Min ' + str(self.x2_min + seg_band2) +
        #               ' < ' + str(self.x2_dist) + ' < ' +
        #               str(self.x2_max - seg_band2) + ' Voltatge '
        #               + str(self.MotorVoltatge[2]))

        #j0
        #possitive voltatge decrese dist
        if ((self.x1_min + seg_band1) >= self.x1_dist):
            #inferior limit
            if(self.MotorVoltatge[1] > 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 0 in the min Limit ' +
                             str(self.x1_dist) + ' <= ' +
                             str(self.x1_min + seg_band1))
                self.joint_min_limit[0] = True
        elif ((self.x1_max - seg_band1) <= self.x1_dist):
            #superior limit
            if(self.MotorVoltatge[1] < 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 0 in the max Limit ' +
                             str(self.x1_dist) + ' >= ' +
                             str(self.x1_max - seg_band1))
                self.joint_max_limit[0] = True
        #####################################
        #j1
        #possitive voltatge increas dist
        if ((self.x0_min + seg_band0) >= self.x0_dist):
            #inferior limit
            if(self.MotorVoltatge[0] > 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 1 in the min Limit ' +
                             str(self.x0_dist) + ' <= ' +
                             str(self.x0_min + seg_band0))
                self.joint_min_limit[1] = True
        elif ((self.x0_max - seg_band0) <= self.x0_dist):
            #superior limit
            if(self.MotorVoltatge[0] < 0):
                self.MotorVoltatge[0] = 0
                #self.MotorVoltatge[1] = 0
                #self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 1 i the max Limit ' +
                             str(self.x0_dist) + ' >= ' +
                             str(self.x0_max - seg_band0))
                self.joint_max_limit[1] = True
            #superior limit

        #####################################
        #j2
        #negative voltatge increas dist
        if ((self.x2_min + seg_band2) >= self.x2_dist):
            #inferior limit
            if(self.MotorVoltatge[2] > 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 2 in the min Limit ' +
                             str(self.x2_dist) + ' <= ' +
                             str(self.x2_min + seg_band2))
                self.joint_min_limit[2] = True

        elif ((self.x2_max - seg_band2) <= self.x2_dist):
            #superior limit
            if(self.MotorVoltatge[2] < 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 2 in the max Limit ' +
                             str(self.x2_dist) + ' >= ' +
                             str(self.x2_max - seg_band2))
                self.joint_max_limit[2] = True
        #####################################
        if ((-np.pi + seg_band3) >= self.roll_value):
            #inferior limit
            if(self.MotorVoltatge[3] > 0):
                self.MotorVoltatge[3] = 0
                rospy.logerr('Joint 3 in the min Limit ' +
                             str(self.roll_value) + ' <= ' +
                             str(-np.pi + seg_band3))
                self.joint_min_limit[3] = True

        elif ((np.pi - seg_band3) <= self.roll_value):
            #superior limit
            if(self.MotorVoltatge[3] < 0):
                self.MotorVoltatge[3] = 0
                rospy.logerr('Joint 3 in the max Limit ' +
                             str(self.roll_value) + ' >= ' +
                             str(np.pi - seg_band3))
                self.joint_max_limit[3] = True

#movement_required
    def move_joints_v2(self):
        #New order updated
        self.lock_voltatge.acquire()
        try:
            if self.CommandVoltatge:
                #rospy.loginfo('Voltage Control')
                #chekif some joint is in the limit and put Zero voltatge
                self.checkJntLimitVoltage()
                self.requestMsg = self.generateVoltatgeMsg()
                try:
                    self.sp.write(self.requestMsg)
                    #To empty the buffer is needed to read the data
                    self.sp.read(51)
                except serial.serialutil.SerialTimeoutException:
                    rospy.loginfo('Error Writing Serial Port')
                    self.openSerialPort()
                    rospy.loginfo('Serial Port Reopened')
                    self.sp.read(51)

        finally:
            self.lock_voltatge.release()

    def generatePoseMsg(self):
        SOM = '\xE7'
        MasterData = '\xFF\xFF\x00'
        MotorMsgHeader = '\x00\x05'
        msg = SOM + MasterData
        MotorMsgTail = '\x00'
        for i in xrange(self.num_dof):
                msg = (msg + MotorMsgHeader +
                       chr(int(self.MotorPosition[i]) / 256) +
                       chr(int(self.MotorPosition[i]) % 256) +
                       chr(int(self.speed_limit[i]) / 256) +
                       chr(int(self.speed_limit[i]) % 256) +
                       chr(int(self.current_limit[i]) / 256) +
                       chr(int(self.current_limit[i]) % 256) + MotorMsgTail)
        CHK = self.checksumHex(msg)
        EOM = '\xE5'
        msg = msg + CHK + EOM
        return msg

    def generateSpeedMsg(self):
        SOM = '\xE7'
        MasterData = '\xFF\xFF\x00'
        MotorMsgHeaderPos = '\x00\x03'
        MotorMsgHeaderNeg = '\x00\x04'
        msg = SOM + MasterData
        MotorMsgTail = '\x00'
        #self.lock_speed.acquire()
        #try:
        for i in xrange(self.num_dof):
            if self.MotorSpeed[i] > 0:
                msg = (msg + MotorMsgHeaderPos +
                       chr(int(self.MotorSpeed[i]) / 256) +
                       chr(int(self.MotorSpeed[i]) % 256))
                #+ MotorMsgTail
            else:
                msg = (msg + MotorMsgHeaderNeg +
                       chr(int(abs(self.MotorSpeed[i])) / 256) +
                       chr(int(abs(self.MotorSpeed[i])) % 256))
                #+ MotorMsgTail
            msg = (msg + chr(int(self.speed_limit[i]) / 256) +
                   chr(int(self.speed_limit[i]) % 256) +
                   chr(int(self.current_limit[i]) / 256) +
                   chr(int(self.current_limit[i]) % 256) + MotorMsgTail)
                #finally:
        #    self.lock_speed.release()
        CHK = self.checksumHex(msg)
        EOM = '\xE5'
        msg = msg + CHK + EOM
        return msg

    def generateVoltatgeMsg(self):
        SOM = '\xE7'
        MasterData = '\xFF\xFF\x00'
        MotorMsgHeaderPos = '\x00\x01'
        MotorMsgHeaderNeg = '\x00\x02'
        MotorMsgTail = '\x00'
        msg = SOM + MasterData
        for i in xrange(self.num_dof):
            if self.MotorVoltatge[i] > 0:
                msg = (msg + MotorMsgHeaderPos +
                       chr(int(self.MotorVoltatge[i]) / 256) +
                       chr(int(self.MotorVoltatge[i]) % 256))
                       # + MotorMsgTail
            else:
                msg = (msg + MotorMsgHeaderNeg +
                       chr(int(abs(self.MotorVoltatge[i])) / 256) +
                       chr(int(abs(self.MotorVoltatge[i])) % 256))
                       # + MotorMsgTail
            msg = (msg + chr(int(self.speed_limit[i]) / 256) +
                   chr(int(self.speed_limit[i]) % 256) +
                   chr(int(self.current_limit[i]) / 256) +
                   chr(int(self.current_limit[i]) % 256) + MotorMsgTail)

        CHK = self.checksumHex(msg)
        EOM = '\xE5'
        msg = msg + CHK + EOM
        return msg

    def generatePIDMsg(self):
        #generates the message to send the PID configuration to all motors
        #this command is use to retrive the basic information
        SOM = '\xE7'
        MasterData = '\xFF\xFF\x00'
        MotorMessageHeader = '\x01'
        MotorMessageTail = '\x00\x00'
        EOM = '\xE5'
        message = SOM+MasterData
        #hex convert int to hex
        #chr convert int to char
        #int base=16 convert str to
        for i in xrange(self.num_dof):
#            rospy.loginfo("Coses " + chr(self.pid_pose_p[i] ) )
            message = (message + MotorMessageHeader + chr(self.pid_pose_p[i]) +
                       chr(self.pid_pose_i[i]) + chr(self.pid_pose_d[i]) +
                       chr(self.pid_speed_p[i]) + chr(self.pid_speed_i[i]) +
                       chr(self.pid_speed_d[i]) + MotorMessageTail)
        CHK = self.checksumHex(message)
        message = message+CHK+EOM
        return message

    def readAnswerArm(self, line):
        rospy.loginfo('Line ' + str(line))
        header = 231
        #header = E7
        tail = 229
        #tail = E5
        present_motor = 0
        for i in xrange(len(line)):
            if i == 0:
                if ord(line[i]) != header:
                    rospy.logerr(
                        'Error reading the message received from the Arm')
                    self.sp.close()
                    self.openSerialPort()
                    return -1
                    #break
            elif i == 1:
                self.temp = (((ord(line[i]) / 255.0) * 3.3) /
                             0.0066101694915254237288135593220339)
            elif i == 2:
                self.voltatge = (((ord(line[i]) / 255.0) * 3.3) /
                                 (6800.0/111500.0))
            elif i == 3:
                self.current = (((((ord(line[i]) / 511.0) * 3.3) /
                                  (39.0/59.0)) / 0.625)*6.0 - 0.2)
            elif i == 49:
                #checksum
                pass
            elif i == 9*present_motor + 4:
                if ord(line[i]) != 1:
                    rospy.logerr('Error reading the message received from the Arm Inside Bucle')
                    rospy.loginfo('value line ' + str(ord(line[i])) +
                                  ' Motor number ' + str(present_motor))
                    #TODO
                    self.sp.close()
                    self.openSerialPort()
                    return -1
                    #break

                #This is to compute the position
                new_position = (ord(line[i+2])*256) + ord(line[i+1])
                difference = new_position - self.PresentPosition[present_motor]
                MAX_ENCODER = 65535
                #Standard
                if not self.firstReadingEncoders:
                    self.lock_encoder.acquire()
                    try:
                        s = ''
                        if abs(difference) < self.max_step:
                            s = (str(rospy.get_time()) + ', ' +
                                 str(new_position) + ', ' +
                                 str(self.PresentPosition[present_motor]) + ', ' +
                                 str(difference) + ', ' +
                                 str(self.AbsolutePose[present_motor] + difference) + ', ' +
                                 str(self.AbsolutePose[present_motor]) + ', ' +
                                 'Normal' )
                            self.AbsolutePose[present_motor] += difference
                        else:
                            difference = 0
                            s = 'NoLimit'
                            #Upper limit
                            if self.PresentPosition[present_motor] + self.max_step >= MAX_ENCODER:
                                difference = (MAX_ENCODER - self.PresentPosition[present_motor]) + new_position
                                s = 'UpperLimit'
                            #Lower limit
                            elif self.PresentPosition[present_motor] - self.max_step < 0:
                                difference = (MAX_ENCODER - new_position) - self.PresentPosition[present_motor]
                                s = 'LowerLimit'
                            if abs(difference) < self.max_step:
                                s = (str(rospy.get_time()) + ', ' +
                                     str(new_position) + ', ' +
                                     str(self.PresentPosition[present_motor]) + ', ' +
                                     str(difference) + ', ' +
                                     str(self.AbsolutePose[present_motor] + difference) + ', ' +
                                     str(self.AbsolutePose[present_motor]) + ', ' +
                                     s )
                                self.AbsolutePose[present_motor] += difference
                            else:
                                s = (str(rospy.get_time()) + ', ' +
                                     str(new_position) + ', ' +
                                     str(self.PresentPosition[present_motor]) + ', ' +
                                     str(difference) + ', ' +
                                     str(self.AbsolutePose[present_motor] + difference) + ', ' +
                                     str(self.AbsolutePose[present_motor]) + ', ' +
                                     'BiggerMaxStep')
                        if (present_motor >=4):
                            self.file_complete.write(s+'\n')
                        else:
                            self.file_complete.write(s+', ')
                    finally:
                        self.lock_encoder.release()
                else:
                    self.firstReadingEncoders = False
                self.PresentPosition[present_motor] = new_position
                self.PresentSpeed[present_motor] = ((ord(line[i+4])*256) +
                                                    ord(line[i+3]))
                #Conversion from rpm to radian per second
                # self.PresentSpeed[present_motor] = (((self.PresentSpeed[present_motor]
                #                                       * self.real_speed_limit[present_motor])
                #                                      / self.speed_limit[present_motor])
                #                                     *60)
                self.PresentCurrent[present_motor] = ((((((ord(line[i+5])) /
                                                          511.0) * 3.3) /
                                                        (39.0/59.0)) / 0.625) *
                                                      6.0 - 0.2)
                #This is not in the documentation free interpretation
                self.PresentForce[present_motor] = ord(line[i+7])
                #This is not in the documentation extracted experimentally
                self.MotorTemp[present_motor] = (((ord(line[i+6]) / 255.0) *
                                                  3.3) /
                                                 0.0066101694915254237288135593220339)
                present_motor += 1
            elif i == 50:
                if ord(line[i]) != tail:
                    rospy.logerr('Error reading the message, the end message is not correct ')
                    return -1
            else:
                pass
#        rospy.loginfo('Absolute position Compute '+ str(self.AbsolutePose) )
        #rospy.loginfo('Foces : ' + str(self.PresentForce))
        #rospy.loginfo('Real position Compute '+ str(self.PresentPosition) )
        if self.debug_files:
            self.file_read_encoder.write(str(rospy.get_time()) + ', ' +
                                         str(self.PresentPosition[0]) + ', ' +
                                         str(self.PresentPosition[1]) + ', ' +
                                         str(self.PresentPosition[2]) + ', ' +
                                         str(self.PresentPosition[3]) + '\n')
            self.file_absolute_encoder.write(str(rospy.get_time()) + ', ' +
                                             str(self.AbsolutePose[0]) + ', ' +
                                             str(self.AbsolutePose[1]) + ', ' +
                                             str(self.AbsolutePose[2]) + ', ' +
                                             str(self.AbsolutePose[3]) + '\n')

        # rospy.loginfo('Global Status : Temp ' + str(self.temp) +
        #               ' Voltatge  ' + str(self.voltatge) +
        #               ' Current ' + str(self.current))
        # rospy.loginfo(' Position Motor 0: ' + str(self.PresentPosition[0]) +
#        rospy.loginfo("Current State " + str(self.AbsolutePose[3]) + " Time " + str(rospy.get_time()))
#        rospy.loginfo("Current State " + str(self.AbsolutePose[3]) + "Time " + str(rospy.get_time())
        #               ' Speed ' + str(self.PresentSpeed[0]) +
        #               ' Current ' + str(self.PresentCurrent[0]) +
        #               ' Temp ' + str(self.MotorTemp[0]) +
        #               ' Force ' + str(self.PresentForce[0]))
        # rospy.loginfo('Vector of Currents ' + str(self.PresentCurrent) )
        # rospy.loginfo('Vector of temps ' + str(self.MotorTemp) )
        return 0

    def checksumHex(self, message):
        check = '\x00'
        for i in message:
            hexeNewValue = hex(ord(i)).replace('0x', '')
            hexeCheckSumValue = hex(ord(check)).replace('0x', '')
            check = chr((int(hexeCheckSumValue, base=16) +
                         int(hexeNewValue, base=16)) % 256)
        return check

if __name__ == '__main__':
    try:
        rospy.init_node('cspi_e5_arm')
        csip_e5_arm = csipE5Arm(rospy.get_name())
        csip_e5_arm.initInternalValues()
        if csip_e5_arm.calibration_needed:
            csip_e5_arm.calibrate()
            rospy.loginfo('Calibration done')
        else:
            csip_e5_arm.loadCalibration()
            rospy.loginfo('Calibration loaded')
        rospy.loginfo('The Arm is Ready to be use')
        csip_e5_arm.run()
        #csip_e5_arm.test_pid()
    except rospy.ROSInterruptException:
        rospy.logerr('The cspi_e5_arm has stopped unexpectedly')
