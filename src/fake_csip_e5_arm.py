#!/usr/bin/env python

# ROS imports
import rospy

#use to load the configuration function
from cola2_lib import cola2_ros_lib

#import empty service to set the 0 position and the valve position
from std_srvs.srv import Empty, EmptyResponse

import numpy as np

from sensor_msgs.msg import JointState
#Publish the values readed from the master
from csip_e5_manipulator.msg import CsipE5ArmState
import threading


class fakeCsipE5Arm:
    def __init__(self, name):
        self.name = name
        
        # Add by Narcis, is ok? #
        self.num_dof = 5
        self.AbsolutePose = np.zeros(self.num_dof)
        #########################
        
        self.getConfig()
        #Publish the valve position
        self.current_pose = np.zeros(5)
        self.lock_pose = threading.Lock()
        self.lock_voltatge = threading.Lock()
        self.MotorVoltatge = np.zeros(5)
        self.computeParametersCalibration()
        self.x0_dist = 0.0
        self.x1_dist = 0.0
        self.x2_dist = 0.0
        self.joint_max_limit = [False,  False, False, False, False]
        self.joint_min_limit = [False,  False, False, False, False]
        self.pub_joint_state = rospy.Publisher("/csip_e5_arm/joint_state",
                                               JointState,
                                               queue_size = 2)
        #Publish the configuration of the valve position Need to be configured
        self.pub_joint_valve = rospy.Publisher(
            "/csip_e5_arm/joint_state_valve",
            JointState,
            queue_size = 2)

        #Publish the distance of the valve position
        self.pub_joint_dist = rospy.Publisher(
            "/csip_e5_arm/joint_state_dist",
            JointState,
            queue_size = 2)

        #Publish the distance of the valve position
        self.pub_joint_cnst = rospy.Publisher(
            "/csip_e5_arm/joint_state_coef",
            JointState,
            queue_size = 2)

        self.pub_arm_state = rospy.Publisher(
            '/csip_e5_arm/arm_state',
            CsipE5ArmState,
            queue_size = 2)

        #Update the new command in voltatge
        rospy.Subscriber('/csip_e5_arm/joint_voltage_command',
                         JointState,
                         self.update_voltatge_command,
                         queue_size = 1)

        #Update the new command in Speed
        rospy.Subscriber('/csip_e5_arm/joint_speed_command',
                         JointState,
                         self.update_speed_command,
                         queue_size = 1)

        #Publish and Subscribe Information of the UWSim
        #Publish the new position to the arm
        self.pub_joint_command = rospy.Publisher('/uwsim/joint_state_command',
                                               JointState,
                                               queue_size = 2)
        #Subscribe to the old position
        rospy.Subscriber('/uwsim/joint_state',
                         JointState,
                         self.update_pose,
                         queue_size = 1)

        #service to set the Zero Position
        self.set_zero_srv = rospy.Service('/csip_e5_arm/set_zero_pose',
                                          Empty, self.setZeroPose)

        self.set_roll_srv = rospy.Service('/csip_e5_arm/set_roll_zero',
                                          Empty, self.setRollZero)

        self.arm_calibration_srv = rospy.Service('/csip_e5_arm/arm_calibration',
                                                 Empty, self.armCalibrationSrv)

    def getConfig(self):
        param_dict = {'joints_enable': '/fake_csip_e5_arm/joints_enable',
                      'period': '/fake_csip_e5_arm/period',
                      'joints_enable': '/fake_csip_e5_arm/joints_enable',
                      'joint_speed_sim': 'fake_csip_e5_arm/joint_speed_sim',
                      'max_voltatge': '/fake_csip_e5_arm/maxVoltatge',
                      'min_voltatge': '/fake_csip_e5_arm/minVoltatge',
                      'r0' : 'csip_e5_arm/r0',
                      'R0': 'csip_e5_arm/R0',
                      'x0_min':'csip_e5_arm/x0_min',
                      'x0_max': 'csip_e5_arm/x0_max' ,
                      'q0_min':'csip_e5_arm/q0_min',
                      'q0_max' : 'csip_e5_arm/q0_max',
                      'r1' : 'csip_e5_arm/r1',
                      'R1': 'csip_e5_arm/R1',
                      'x1_min':'csip_e5_arm/x1_min',
                      'x1_max': 'csip_e5_arm/x1_max' ,
                      'q1_min':'csip_e5_arm/q1_min',
                      'q1_max' : 'csip_e5_arm/q1_max',
                      'r2' : 'csip_e5_arm/r2',
                      'R2': 'csip_e5_arm/R2',
                      'x2_min':'csip_e5_arm/x2_min',
                      'x2_max': 'csip_e5_arm/x2_max' ,
                      'q2_min':'csip_e5_arm/q2_min',
                      'q2_max' : 'csip_e5_arm/q2_max'
                      }
        cola2_ros_lib.getRosParams(self, param_dict)

    def computeParametersCalibration(self):
        self.q0_min_val = (np.pi/180)*(sum(self.q0_min))/len(self.q0_min)
        self.q0_max_val = (np.pi/180)*(sum(self.q0_max))/len(self.q0_max)
        self.q0bar_min = np.pi-self.q0_min_val-np.arccos((self.r0**2+self.R0**2-self.x0_max**2)/(2*self.r0*self.R0))
        self.q0bar_max = np.pi-self.q0_max_val-np.arccos((self.r0**2+self.R0**2-self.x0_min**2)/(2*self.r0*self.R0))
        self.q0bar = (self.q0bar_min+self.q0bar_max)/2

        self.q1_min_val = (np.pi/180)*(sum(self.q1_min))/len(self.q1_min)
        self.q1_max_val = np.pi-(np.pi/180)*(sum(self.q1_max))/len(self.q1_min)
        self.q1bar_min = np.pi-self.q1_min_val-np.arccos((self.r1**2+self.R1**2-self.x1_max**2)/(2*self.r1*self.R1))
        self.q1bar_max = np.pi-self.q1_max_val-np.arccos((self.r1**2+self.R1**2-self.x1_min**2)/(2*self.r1*self.R1))
        self.q1bar = (self.q1bar_min+self.q1bar_max)/2.0

        self.q2_min_val = -np.pi/2.0 + (np.pi/180)*(sum(self.q2_min))/len(self.q2_min)
        self.q2_max_val = np.pi/2.0 - (np.pi/180)*(sum(self.q2_max))/len(self.q2_max)
        self.q2bar_min = np.pi-self.q2_min_val+np.arccos((self.r2**2+self.R2**2-self.x2_max**2)/(2*self.r2*self.R2))
        self.q2bar_max = np.pi-self.q2_max_val+np.arccos((self.r2**2+self.R2**2-self.x2_min**2)/(2*self.r2*self.R2))
        self.q2bar = (self.q2bar_min+self.q2bar_max)/2.0

    def setZeroPose(self, req):
        self.AbsolutePose = np.zeros(self.num_dof)
        rospy.loginfo('Absoulte pose initialized')
        return EmptyResponse()

    def setRollZero(self, req):
        rospy.loginfo('The Roll is setted Zero')
        self.AbsolutePose[3] = 0.0
        return EmptyResponse()

    def update_voltatge_command(self, data):
        self.lock_voltatge.acquire()
        try:
            self.MotorVoltatge = np.array([data.position[0], data.position[1],
                                           data.position[2], -1.0*data.position[3],
                                           data.position[4]])
            for i in range(5):
                if (self.MotorVoltatge[i] > self.max_voltatge):
                    self.MotorVoltatge[i] = self.max_voltatge
                elif (self.MotorVoltatge[i] < -1*self.max_voltatge):
                    self.MotorVoltatge[i] = -1*self.max_voltatge
        finally:
            self.lock_voltatge.release()

    def update_speed_command(self, data):
        self.lock_voltatge.acquire()
        try:
            self.MotorVoltatge = np.array([data.position[0], data.position[1],
                                           data.position[2], -1.0*data.position[3],
                                           data.position[4]])
            for i in range(5):
                if (self.MotorVoltatge[i] > self.max_voltatge):
                    self.MotorVoltatge[i] = self.max_voltatge
                elif (self.MotorVoltatge[i] < -1*self.max_voltatge):
                    self.MotorVoltatge[i] = -1*self.max_voltatge
        finally:
            self.lock_voltatge.release()

    def update_pose(self, data):
        self.lock_pose.acquire()
        try:
            self.current_pose = np.array([data.position[0], data.position[1],
                                          data.position[2], data.position[3],
                                          data.position[4]])
        finally:
            self.lock_pose.release()

    def publish_joints(self):
        self.lock_pose.acquire()
        try:
            joint_state = JointState()
            joint_state.position = self.current_pose.tolist()
            joint_state.velocity = np.zeros(5).tolist()
            joint_state.effort = np.zeros(5).tolist()
            joint_state.header.stamp = rospy.get_rostime()
            self.pub_joint_state.publish(joint_state)

            arm_state = CsipE5ArmState()
            arm_state.header.stamp = rospy.get_rostime()
            arm_state.temperature = 37.0
            arm_state.voltatge = 1.0
            arm_state.current = 0.6
            arm_state.joint_max_limit = self.joint_max_limit
            arm_state.joint_min_limit = self.joint_min_limit
            self.pub_arm_state.publish(arm_state)

            #Compute the distance to obtain the factor
            #used in the control using the velocities

            q0 = self.current_pose[0]
            q1 = self.current_pose[1]
            q2 = self.current_pose[2]
            # self.x1_dist = np.sqrt(-1*((np.cos(-q1-np.pi-self.q0bar)*2*self.r0*self.R0)-(self.r0**2)-(self.R0**2)))
            # self.x0_dist = np.sqrt(-1*((np.cos(-q0-np.pi-self.q1bar)*2*self.r1*self.R1)-(self.r1**2)-(self.R1**2)))
            # self.x2_dist = np.sqrt(-1*((np.cos(q2-np.pi+self.q2bar)*2*self.r2*self.R2)-(self.r2**2)-(self.R2**2)))
            self.x1_dist = np.sqrt(-1*(np.cos(np.pi -q1 -self.q0bar)*2*self.r0*self.R0)+self.r0**2+self.R0**2)
            self.x0_dist = np.sqrt(-1*(np.cos(np.pi -q0 -self.q1bar)*2*self.r1*self.R1)+self.r1**2+self.R1**2)
            self.x2_dist = np.sqrt(-1*(np.cos(q2 + self.q2bar -np.pi)*2*self.r2*self.R2)+self.r2**2+self.R2**2)
            joint_dist = JointState()
            joint_dist.header.stamp = rospy.get_rostime()
            joint_dist.position = [self.x0_dist, self.x1_dist,
                                   self.x2_dist, 0.0, 0.0]
            self.pub_joint_dist.publish(joint_dist)
            x0_value = ((self.R0*self.r0)*(np.sqrt(1-((self.R0**2+self.r0**2-self.x0_dist**2)/(2*self.R0*self.r0))**2)))/self.x0_dist
            x1_value = ((self.R1*self.r1)*(np.sqrt(1-((self.R1**2+self.r1**2-self.x1_dist**2)/(2*self.R1*self.r1))**2)))/self.x1_dist
            x2_value = ((self.R2*self.r2)*(np.sqrt(1-((self.R2**2+self.r2**2-self.x2_dist**2)/(2*self.R2*self.r2))**2)))/self.x2_dist
            joint_cnst = JointState()
            joint_cnst.header.stamp = rospy.get_rostime()
            joint_cnst.position = [x0_value, x1_value, x2_value, 0.0, 0.0]
            self.pub_joint_cnst.publish(joint_cnst)
        finally:
            self.lock_pose.release()

    def checkJntLimitVoltage(self):
        #segurity band for each joint 5 %
        self.joint_max_limit = [False,  False, False, False, False]
        self.joint_min_limit = [False,  False, False, False, False]

        seg_band0 = (self.x0_max - self.x0_min)*0.05
        seg_band1 = (self.x1_max - self.x1_min)*0.05
        seg_band2 = (self.x2_max - self.x2_min)*0.05
        #limit angle
        seg_band3 = (2*np.pi)*0.05
        #j0
        #possitive voltatge decrese dist
        if ((self.x1_min + seg_band1) >= self.x1_dist):
            #inferior limit
            if(self.MotorVoltatge[1] > 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 1 in the min Limit ' +
                             str(self.x1_dist) + ' <= ' +
                             str(self.x1_min + seg_band1))
                self.joint_min_limit[1] = True
        elif ((self.x1_max - seg_band1) <= self.x1_dist):
            #superior limit
            if(self.MotorVoltatge[1] < 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 1 in the max Limit ' +
                             str(self.x1_dist) + ' >= ' +
                             str(self.x1_max - seg_band1))
                self.joint_max_limit[1] = True
        #####################################
        #j1
        #possitive voltatge increas dist
        if ((self.x0_min + seg_band0) >= self.x0_dist):
            #inferior limit
            if(self.MotorVoltatge[0] > 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 0 in the min Limit ' +
                             str(self.x0_dist) + ' <= ' +
                             str(self.x0_min + seg_band0))
                self.joint_min_limit[0] = True
        elif ((self.x0_max - seg_band0) <= self.x0_dist):
            #superior limit
            if(self.MotorVoltatge[0] < 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 0 i the max Limit ' +
                             str(self.x0_dist) + ' >= ' +
                             str(self.x0_max - seg_band0))
                self.joint_max_limit[0] = True
            #superior limit
        #####################################
        #j2
        #negative voltatge increas dist
        if ((self.x2_min + seg_band2) >= self.x2_dist):
            #inferior limit
            if(self.MotorVoltatge[2] < 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 2 in the min Limit ' +
                             str(self.x2_dist) + ' <= ' +
                             str(self.x2_min + seg_band2))
                self.joint_min_limit[2] = True
        elif ((self.x2_max - seg_band2) <= self.x2_dist):
            #superior limit
            if(self.MotorVoltatge[2] > 0):
                self.MotorVoltatge[0] = 0
                self.MotorVoltatge[1] = 0
                self.MotorVoltatge[2] = 0
                rospy.logerr('Joint 2 in the max Limit ' +
                             str(self.x2_dist) + ' >= ' +
                             str(self.x2_max - seg_band2))
                self.joint_max_limit[2] = True
        #####################################
        # j3
        # Limit the roll values trying to avoid to have the 360
        #if ( (self.current_pose[3] + seg_band3) >= np.pi ):
        #print 'Current roll ' + str(self.current_pose[3]) + ' Limit Min ' + str(-np.pi+seg_band3) + ' Limit Max '+ str(np.pi+seg_band3)
        if ((-np.pi + seg_band3) >= self.current_pose[3]):
            #rospy.loginfo('Value of the Roll ' + str(self.current_pose[3]) )
            #superior limit
            if ( self.MotorVoltatge[3] < 0 ) :
                self.MotorVoltatge[3] = 0
                rospy.logerr('Joint 3 in the min Limit')
                self.joint_min_limit[3] = True
        #elif ( (self.current_pose[3] - seg_band3) <= -np.pi ):
        elif ((np.pi - seg_band3) <= self.current_pose[3]):
            #rospy.loginfo('Value of the Roll ' + str(self.current_pose[3]) )
            #inferior limit
            if ( self.MotorVoltatge[3] > 0 ) :
                self.MotorVoltatge[3] = 0
                rospy.logerr('Joint 3 in the max Limit')
                self.joint_max_limit[3] = True

    def move_joints(self):
        inc_angle = np.zeros(5)
        self.lock_voltatge.acquire()
        try:
            inc_angle = np.copy(self.MotorVoltatge)
            for i in range(5):
                if (abs(inc_angle[i]) < self.min_voltatge):
                    inc_angle[i] = 0.0
            inc_angle = inc_angle/self.max_voltatge
            inc_angle = inc_angle*np.asarray(self.joint_speed_sim)*self.period
        finally:
            self.lock_voltatge.release()
        self.lock_pose.acquire()
        try:
            new_pose = inc_angle + self.current_pose

            joint_command = JointState()
            #joint_command.name = ['baselink_to_base', 'base_to_shoulder', 'shoulder_to_biceps', 'wrist', 'palm_to_jawS']
            joint_command.position = new_pose.tolist()
            joint_command.velocity = np.zeros(5).tolist()
            joint_command.effort = np.zeros(5).tolist()
            joint_command.header.stamp = rospy.get_rostime()
            self.pub_joint_command.publish(joint_command)
        finally:
            self.lock_pose.release()

    def armCalibrationSrv(self, req):
        """
        This method call the function to calibrate the arm.
        """
        rospy.sleep(120.0)
        return EmptyResponse()

    def run(self):
        while not rospy.is_shutdown():
            self.publish_joints()
            self.checkJntLimitVoltage()
            self.move_joints()
            rospy.sleep(self.period)

if __name__ == '__main__':
    try:
        rospy.init_node('fake_csip_e5_arm')
        fake_csip_e5_arm = fakeCsipE5Arm(rospy.get_name())
        fake_csip_e5_arm.run()
    except rospy.ROSInterruptException:
        rospy.logerr('The fake_cspi_e5_arm has stopped unexpectedly')
