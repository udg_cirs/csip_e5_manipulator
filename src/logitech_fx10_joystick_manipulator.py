#! /usr/bin/env python

#  logitech f510 joystick driver
#  Created on: 17/06/2011
#  Updated on: 11/01/2012
#  Author: narcis

# ROS imports
import rospy

# import messages
from sensor_msgs.msg import Joy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped

#use to load the configuration function
#import cola2_lib
from cola2_lib import cola2_lib, cola2_ros_lib
import threading

# UVMS
from geometry_msgs.msg import Twist

import numpy as np
import tf


class LogitechFX10Joystick:
    def __init__(self, name):
        self.name = name
        self.getConfig()

        self.working_mode = 5
        self.prev_working_mode = 5
        self.slow_mode_on = False

        self.lock_dist_auv = threading.Lock()
        # Create publisher
        self.pub_joy_data = rospy.Publisher(
            "/cola2_control/map_ack_data", 
            Joy,
            queue_size = 2)
            
        self.pub_joy_ack_teleop = rospy.Publisher(
            "/cola2_control/map_ack_ack", 
            String,
            queue_size = 2)
            
        self.pub_volt = rospy.Publisher(
            "/cola2_control/joystick_arm_volt", 
            Joy,
            queue_size = 2)
            
        self.pub_jnt = rospy.Publisher(
            "/cola2_control/joystick_arm_jnt", 
            Joy,
            queue_size = 2)
            
        self.pub_ef = rospy.Publisher(
            "/cola2_control/joystick_arm_ef", 
            Joy,
            queue_size = 2)
            
        self.pub_ef_vel = rospy.Publisher(
            "/cola2_control/joystick_arm_ef_vel", 
            Joy,
            queue_size = 2)
            
        self.pub_dist = rospy.Publisher(
            "/cola2_control/distance_auv_valve", 
            PoseStamped,
            queue_size = 2)
            
        self.pub_uvms = rospy.Publisher(
            "/cola2_control/uvms_vel_cmd", 
            Twist,
            queue_size = 2)
            

        # Create Subscriber
        rospy.Subscriber("joy",
                         Joy,
                         self.updateJoy,
                         queue_size = 1)
        rospy.Subscriber("/cola2_control/map_ack_ok",
                         String,
                         self.updateAck,
                         queue_size = 1)
        rospy.Subscriber(
            '/cola2_control/valve_dist',
            PoseStamped,
            self.updatePoseAuv,
            queue_size = 1)

        #Create Timer
        rospy.Timer(rospy.Duration(0.1), self.pubJoyData)

        #Create Joy msg
        self.joy_data = Joy()
        [self.joy_data.axes.append(0) for i in range(8)]
        [self.joy_data.buttons.append(0) for i in range(11)]
        rospy.loginfo("%s, initialized", self.name)

        self.dist_auv_valve = PoseStamped()

    def getConfig(self):
        param_dict = {'min_x_v': '/joy_arm/min_x_v',
                      'max_x_v': '/joy_arm/max_x_v',
                      'min_y_v': '/joy_arm/min_y_v',
                      'max_y_v': '/joy_arm/max_y_v',
                      'min_z_v': '/joy_arm/min_z_v',
                      'max_z_v': '/joy_arm/max_z_v',
                      'min_roll_v': '/joy_arm/min_roll_v',
                      'max_roll_v': '/joy_arm/max_roll_v',
                      'min_pitch_v': '/joy_arm/min_pitch_v',
                      'max_pitch_v': '/joy_arm/max_pitch_v',
                      'min_yaw_v': '/joy_arm/min_yaw_v',
                      'max_yaw_v': '/joy_arm/max_yaw_v'
                      }
        cola2_ros_lib.getRosParams(self, param_dict)

    def pubJoyData(self, event):
        if len(self.joy_data.buttons) > 1:
            if self.working_mode == 5:
                #self.pub_joy_data.publish(self.joy_data)
                self.pubMapAckData(self.joy_data)
            elif self.working_mode == 0:
                self.pub_volt.publish(self.joy_data)
            elif self.working_mode == 1:
                self.pub_jnt.publish(self.joy_data)
            elif self.working_mode == 2:
                self.pub_ef.publish(self.joy_data)
            elif self.working_mode == 3:
                self.pub_ef_vel.publish(self.joy_data)
            elif self.working_mode == 4:
                self.pubDistAUVValve()
            elif self.working_mode == 6:
                self.pubTwistUVMS(self.joy_data)
            else:
                rospy.loginfo('Mode not implemented')

    def pubMapAckData(self, joy_data):
        map_ack_data_msg = Joy()
        map_ack_data_msg.axes = np.zeros(12)
        map_ack_data_msg.buttons = np.zeros(15)
        map_ack_data_msg.axes[6:12] = joy_data.axes[0:6]
        map_ack_data_msg.header.stamp = rospy.Time().now()
        self.pub_joy_data.publish(map_ack_data_msg)

    def updateJoy(self, data):
        if len(data.axes) > 6:
            # Check if the mode is changed
            if data.axes[2] < 0:
                #rospy.loginfo('Swaping Mode')
                #Button A mode 1 -> arm
                if data.buttons[0] == 1:
                    rospy.loginfo(
                        '#################Using Voltatge Mode###############')
                    self.working_mode = 0
                    #Button X mode 2 -> arm
                elif data.buttons[2] == 1:
                    rospy.loginfo(
                        '#################Using Joint Mode###############')
                    self.working_mode = 1
                    #Button Y mode 3 -> arm
                elif data.buttons[3] == 1:
                    rospy.loginfo(
                        '#################Using Pose Mode###############')
                    self.working_mode = 2
                    #Button B mode 4 -> arm
                elif data.buttons[1] == 1:
                    rospy.loginfo(
                        '#################Using Speed Mode###############')
                    self.working_mode = 3
                    #Button START mode 0 -> auv standard
                elif data.buttons[7] == 1:
                    rospy.loginfo(
                        '#################Using Control Pose AUV###############')
                    self.working_mode = 5
                    #Button BACK mode -> auv panel
                elif data.buttons[6] == 1:
                    rospy.loginfo(
                        '#################Using Control Distance Between AUV and Valve###############')
                    self.working_mode = 4
                elif data.axes[7] == 1:
                    rospy.loginfo(
                    '#################Using UVMS Mode###############')
                    self.working_mode = 6
            elif data.axes[5] < 0.0:
                #special MODE SLOW ROBOT MOTION
                if not self.slow_mode_on:
                    self.prev_working_mode = self.working_mode
                    self.working_mode = 5
                    self.slow_mode_on = True

                self.joy_data.axes[0] = data.axes[1]
                self.joy_data.axes[5] = -data.axes[3]
                self.joy_data.axes[2] = -data.axes[4]
                self.joy_data.axes[1] = -data.axes[0]
                self.joy_data.axes[6] = -data.axes[6]
                self.joy_data.axes[7] = data.axes[7]
                #self.joy_data.axes[3] = -data.axes[3]
                # Copy pressed buttons
                self.joy_data.buttons = list()
                [self.joy_data.buttons.append(b) for b in data.buttons]
                # Joystick setpoints to velocities
                if self.joy_data.axes[0] > 0.0:
                    self.joy_data.axes[0] *= self.max_x_v[6]
                else:
                    self.joy_data.axes[0] *= -self.min_x_v[6]
                if self.joy_data.axes[1] > 0.0:
                    self.joy_data.axes[1] *= self.max_y_v[6]
                else:
                    self.joy_data.axes[1] *= -self.min_y_v[6]
                if self.joy_data.axes[2] > 0.0:
                    self.joy_data.axes[2] *= self.max_z_v[6]
                else:
                    self.joy_data.axes[2] *= -self.min_z_v[6]
                if self.joy_data.axes[5] > 0.0:
                    self.joy_data.axes[5] *= self.max_yaw_v[6]
                else:
                    self.joy_data.axes[5] *= -self.min_yaw_v[6]
                # if self.joy_data.axes[3] > 0.0:
                #     self.joy_data.axes[3] *= self.max_roll_v[6]
                # else:
                #     self.joy_data.axes[3] *= -self.min_roll_v[6]
            elif self.slow_mode_on and data.axes[5] >= 0.0:
                #restore previous working mod
                self.slow_mode_on = False
                self.working_mode = self.prev_working_mode
            else:
#                rospy.loginfo('Usual Mode')
                self.joy_data.axes[0] = data.axes[1]
                if self.working_mode == 5:
                    self.joy_data.axes[5] = -data.axes[3]
                    #self.joy_data.axes[3] = -data.axes[3]
                else:
                    self.joy_data.axes[5] = -data.axes[3]
                    #self.joy_data.axes[3] = data.axes[3]
                self.joy_data.axes[2] = -data.axes[4]
                self.joy_data.axes[1] = -data.axes[0]
                self.joy_data.axes[6] = -data.axes[6]
                self.joy_data.axes[7] = data.axes[7]
                # Copy pressed buttons
                self.joy_data.buttons = list()
                [self.joy_data.buttons.append(b) for b in data.buttons]
                # Joystick setpoints to velocities
                if self.joy_data.axes[0] > 0.0:
                    self.joy_data.axes[0] *= self.max_x_v[self.working_mode]
                else:
                    self.joy_data.axes[0] *= -self.min_x_v[self.working_mode]
                if self.joy_data.axes[1] > 0.0:
                    self.joy_data.axes[1] *= self.max_y_v[self.working_mode]
                else:
                    self.joy_data.axes[1] *= -self.min_y_v[self.working_mode]
                if self.joy_data.axes[2] > 0.0:
                    self.joy_data.axes[2] *= self.max_z_v[self.working_mode]
                else:
                    self.joy_data.axes[2] *= -self.min_z_v[self.working_mode]
                if self.joy_data.axes[5] > 0.0:
                    self.joy_data.axes[5] *= self.max_yaw_v[self.working_mode]
                else:
                    self.joy_data.axes[5] *= -self.min_yaw_v[self.working_mode]
                # if self.joy_data.axes[3] > 0.0:
                #     self.joy_data.axes[3] *= self.max_roll_v[self.working_mode]
                # else:
                #     self.joy_data.axes[3] *= -self.min_roll_v[self.working_mode]
        else:
            rospy.logerr("%s, Invalid /joy message received!", self.name)

    def updateAck(self, ack):
        ack_list = ack.data.split()
        if len(ack_list) == 2 and ack_list[1] == 'ok':
            seq = int(ack_list[0]) + 1
            self.pub_joy_ack_teleop.publish(str(seq) + " ack")
        else:
            rospy.logerr(
                "%s, received invalid teleoperation heart beat!", self.name)

    def updatePoseAuv(self, dist):
        self.lock_dist_auv.acquire()
        try:
            self.dist_auv_valve = dist
        finally:
            self.lock_dist_auv.release()

    def pubDistAUVValve(self):
        self.lock_dist_auv.acquire()
        try:
            if any(np.asarray(self.joy_data.axes) != 0):
                dist = PoseStamped()
                dist.pose.position.x = self.dist_auv_valve.pose.position.x - self.joy_data.axes[0]
                dist.pose.position.y = self.dist_auv_valve.pose.position.y - self.joy_data.axes[1]
                dist.pose.position.z = self.dist_auv_valve.pose.position.z + self.joy_data.axes[2]

                euler = tf.transformations.euler_from_quaternion([dist.pose.orientation.x,
                                                                  dist.pose.orientation.y,
                                                                  dist.pose.orientation.z,
                                                                  dist.pose.orientation.w])
                euler = np.asarray(euler)
                euler[0] += self.joy_data.axes[3]
                euler[1] += self.joy_data.axes[4]
                euler[2] += self.joy_data.axes[5]

                quat = tf.transformations.quaternion_from_euler(
                    euler[0], euler[1], euler[2])

                dist.pose.orientation.x = quat[0]
                dist.pose.orientation.y = quat[1]
                dist.pose.orientation.z = quat[2]
                dist.pose.orientation.w = quat[3]

                dist.header.stamp = rospy.get_rostime()

                self.pub_dist.publish(dist)
        finally:
            self.lock_dist_auv.release()

    def pubTwistUVMS(self, joy_data):
        """
        Transforms the data form the joystick to the twist data needed by
        the UVMS controller
        """
        twist_data = Twist()
        twist_data.linear.x = joy_data.axes[0]
        twist_data.linear.y = -joy_data.axes[1]
        twist_data.linear.z = joy_data.axes[2]

        twist_data.angular.x = joy_data.axes[3]
        twist_data.angular.y = joy_data.axes[4]
        twist_data.angular.z = joy_data.axes[5]

        self.pub_uvms.publish(twist_data)

if __name__ == '__main__':
    try:
        rospy.init_node('logitech_f510_joystick')
        joystick = LogitechFX10Joystick(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
